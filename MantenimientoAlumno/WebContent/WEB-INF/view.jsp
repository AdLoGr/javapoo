<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index</title>
</head>
<body>
<table border=1>
        <thead>
            <tr>
                <th>Alumno Id</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Dni</th>
                <th colspan=1>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${alumnos}" var="alumno">
                <tr>
                    <td><c:out value="${alumno.getIdAlumno()}" /></td>
                    <td><c:out value="${alumno.getNombre()}" /></td>
                    <td><c:out value="${alumno.getApellidos()}" /></td>
                    <td><c:out value="${alumno.getDni()}" /></td>
                    <td><a href="UserController?action=delete&userId=<c:out value="${alumno.getIdAlumno()}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>