function validate(){			
	var list = [
	   document.forms["form"]["id"].value,
	   document.forms["form"]["nombre"].value,
	   document.forms["form"]["apellido"].value,
	   document.forms["form"]["dni"].value
	]
	
	var voidFlag = false;
	list.forEach(function (element, index, array){
		if (element == null || element === "") {
			voidFlag = true;					
		}
	})
		
	if (voidFlag === true) {
		alert("All fields must be filled!");
		return false;
	}
	
	return true;
}