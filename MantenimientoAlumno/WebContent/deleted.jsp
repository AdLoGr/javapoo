<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% if (request.getParameter("removedSuccesful") == null) { %>
		<form id="form" 
		      action="http://localhost:8080/MantenimientoAlumno/Alumno?action=delete" 
			  method="GET">
			ID ALUMNO TO REMOVE: <input type="text" name="idAlumno">
			<input type="submit" value="REMOVE">
		</form>
	<% } else { 
	   		Integer hasBeenRemoved = Integer.parseInt(request.getParameter("removedSuccesful"));
	   		if (hasBeenRemoved == 1) {
	   			out.write("DELETED SUCCESFUL!");
	   		} else {
	   			out.write("DELETED FAILED");
	   		}
	   } %>
</body>
</html>