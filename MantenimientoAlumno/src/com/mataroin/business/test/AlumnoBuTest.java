package com.mataroin.business.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mataroin.business.AlumnoBu;
import com.mataroin.dao.AlumnoDao;
import com.mataroin.dao.IDao;
import com.mataroin.model.Alumno;

public class AlumnoBuTest {
	
	
	private AlumnoBu alumnoBu;
	private Alumno alumno;
	private IDao<Alumno> daoMock;
	private int idAlumno;
	

	@Before
	public void setUp() throws Exception {
		daoMock = mock(AlumnoDao.class);	
		alumnoBu = new AlumnoBu(daoMock);
		alumno = new Alumno();
		idAlumno = 10;
	}

	
	@Test
	public void test() throws Exception {
		AlumnoBu mockedAlumnoBu = mock(AlumnoBu.class);
		alumno = new Alumno();
		idAlumno = 10;
		
		mockedAlumnoBu.add(alumno);
		mockedAlumnoBu.getAll();
		mockedAlumnoBu.getById(idAlumno);
		mockedAlumnoBu.modify(alumno);
		mockedAlumnoBu.remove(idAlumno);
		
		verify(mockedAlumnoBu, times(1)).add(alumno);
		verify(mockedAlumnoBu, times(1)).getAll();
		verify(mockedAlumnoBu, times(1)).getById(idAlumno);
		verify(mockedAlumnoBu, times(1)).modify(alumno);
		verify(mockedAlumnoBu, times(1)).remove(idAlumno);
	}
	
	
	@Test
	public void testThatAddReturnsWhenSuccesful() throws Exception {
		when(daoMock.add(any(Alumno.class)))
	        .thenReturn(anyInt());
		
		alumnoBu.add(new Alumno());
		
		verify(daoMock).add(any(Alumno.class));
	}
	
	@Test(expected = SQLException.class) 
	public void testThatAddThrowsWhenNotSuccesful() throws Exception {
		when(daoMock.add(any(Alumno.class)))
        .thenThrow(new SQLException());
		
		alumnoBu.add(new Alumno());
		
		verify(daoMock).add(any(Alumno.class));
	}
	
	
	@Test
	public void getByIdReturnsAlumoWhenSuccesful() throws Exception {
		when(daoMock.getById(anyInt()))
			.thenReturn(alumno);
		
		Alumno resultAlumno = alumnoBu.getById(alumno.getIdAlumno());
		
		assertEquals(alumno, resultAlumno);
		verify(daoMock).getById(anyInt());
	}
	
	@Test(expected = SQLException.class) 
	public void getByIdThrowsExceptionWhenFailed() throws Exception {
		when(daoMock.getById(anyInt()))
			.thenThrow(new SQLException());
		
		Alumno resultAlumno = alumnoBu.getById(alumno.getIdAlumno());
		
		assertNotEquals(alumno, resultAlumno);
		verify(daoMock).getById(anyInt());
	}
	
	
	@Test
	public void getAllReturnWhenSuccessful() throws Exception {
		List<Alumno> expectedList = new ArrayList<Alumno>();
		when(daoMock.getAll())
			.thenReturn(expectedList);
		
		List<Alumno> resultList = alumnoBu.getAll();
		
		assertEquals(expectedList, resultList);
		verify(daoMock).getAll();
	}
		
	
	@Test
	public void modifyReturnWhenSuccessful() throws Exception {
		when(daoMock.modify(any(Alumno.class)))
			.thenReturn(1);
		
		int modifiedRows = alumnoBu.modify(alumno);
		
		assertTrue(1 == modifiedRows);
		verify(daoMock).modify(any(Alumno.class));
	}
	
	@Test(expected = SQLException.class)  
	public void modifyThrowsWhenFails() throws Exception {
		when(daoMock.modify(any(Alumno.class)))
		.thenReturn(0);
		
		int modifiedRows = alumnoBu.modify(alumno);
		
		assertTrue(0 == modifiedRows);
		verify(daoMock).modify(any(Alumno.class));
	}
	
	
	@Test
	public void removeReturnWhenSuccessful() throws Exception {
		when(daoMock.remove(alumno.getIdAlumno()))
			.thenReturn(1);
		
		int removedRows = alumnoBu.remove(alumno.getIdAlumno());
		
		assertTrue(1 == removedRows);
		verify(daoMock).remove(alumno.getIdAlumno());
	}
	
	@Test(expected = SQLException.class) 
	public void removeThrowsWhenFails() throws Exception {
		when(daoMock.remove(alumno.getIdAlumno()))
			.thenThrow(new SQLException());
		
		int removedRows = alumnoBu.remove(alumno.getIdAlumno());
		
		assertTrue(0 == removedRows);
		verify(daoMock).remove(alumno.getIdAlumno());
	}
	

}
