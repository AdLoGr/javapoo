package com.mataroin.business;


import java.util.List;


public interface IBusiness<T> {
	
	
	int add(T model) throws Exception;
	
	List<T> getAll() throws Exception;
	T getById(int id) throws Exception;
	
	int modify(T model) throws Exception;
	
	
	int remove(int id) throws Exception;
	

}
