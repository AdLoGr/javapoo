package com.mataroin.business;

import java.util.List;

import com.mataroin.dao.AlumnoDao;
import com.mataroin.dao.IDao;
import com.mataroin.model.Alumno;

public class AlumnoBu implements IBusiness<Alumno> {
	
	
	private IDao<Alumno> dao = null;
	
	
	public AlumnoBu() {
		dao = new AlumnoDao();
	}
	
	public AlumnoBu(IDao<Alumno> dao) {
		setDao(dao);
	}
																														
	
	@Override
	public int add(Alumno model) throws Exception {
		return dao.add(model);
	}

	
	@Override
	public List<Alumno> getAll() throws Exception {
		
		return dao.getAll();
	}
	

	@Override
	public Alumno getById(int id) throws Exception {
		
		return dao.getById(id);
	}
	

	@Override
	public int modify(Alumno model) throws Exception {
		
		return dao.modify(model);
	}
	

	@Override
	public int remove(int id) throws Exception {
		return dao.remove(id);
	}

	
	public IDao<Alumno> getDao() {
		return dao;
	}

	
	public void setDao(IDao<Alumno> dao) {
		this.dao = dao;
	}
	
	
}
