package com.mataroin.business.integration.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Test;

import com.mataroin.business.AlumnoBu;
import com.mataroin.business.IBusiness;
import com.mataroin.model.Alumno;

public class AlumnoBuTest {
	
	
	private static int idAlumno;
	

	@Test
	public void testAlumnoBu() throws Exception
	{ 
		AlumnoBu  alumnoBu = new AlumnoBu();
		Field field = alumnoBu.getClass().getDeclaredField("dao");
		
		boolean fieldHasBeenInitialized = field != null;
		assertTrue(fieldHasBeenInitialized);
	}

	
	@Test
	public void testAdd() throws Exception
	{
		Alumno alumno = new Alumno();
		alumno.setNombre("Pepito");
		alumno.setApellidos("Grillo");
		alumno.setDni("97342837S");
			
		IBusiness<Alumno> bu = new AlumnoBu();
		idAlumno = bu.add(alumno);
		
		boolean hasBeenGenAnIdForNewAlumno = idAlumno > 0;
		assertTrue(hasBeenGenAnIdForNewAlumno);	
	}

	
	@Test
	public void testGetAll() throws Exception
	{
		IBusiness<Alumno> bu = new AlumnoBu();
		List<Alumno> alumnos = bu.getAll();
		
		boolean expectedResult = alumnos.size() >= 1;
		assertTrue(expectedResult);
	}

	
	@Test
	public void testGetById() throws Exception
	{
		IBusiness<Alumno> bu = new AlumnoBu();
		Alumno alumno = bu.getById(idAlumno);
		
		boolean idFromReturnedAlumnoIsEqual = alumno.getIdAlumno() == idAlumno;
		assertTrue(idFromReturnedAlumnoIsEqual);
	}

	
	@Test
	public void testModify() throws Exception
	{
		Alumno alumno = new Alumno();
		alumno.setNombre("Michael");
		alumno.setApellidos("Shakespear");
		alumno.setDni("00000312B");
		alumno.setIdAlumno(idAlumno);
		
		IBusiness<Alumno> bu = new AlumnoBu();
		int modifiedRows = bu.modify(alumno);
		
		boolean rowsHaveBeenModified = modifiedRows > 0;
		assertTrue(rowsHaveBeenModified);
	}

	
	@Test
	public void testRemove() throws Exception
	{
		IBusiness<Alumno> bu = new AlumnoBu();
		int removedRows = bu.remove(idAlumno);
		
		boolean rowsHaveBennRemoved = removedRows > 0;
		assertTrue(rowsHaveBennRemoved);
	}

}
