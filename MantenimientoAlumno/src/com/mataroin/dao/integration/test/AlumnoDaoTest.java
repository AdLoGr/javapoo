package com.mataroin.dao.integration.test;


import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import com.mataroin.dao.AlumnoDao;
import com.mataroin.dao.IDao;
import com.mataroin.model.Alumno;

import org.junit.Test;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlumnoDaoTest {
	
	
	public static int idAlumno = 0;

	
	@Test
	public void test1Add() throws SQLException 
	{		
		Alumno alumno = new Alumno();
		alumno.setNombre("Adria");
		alumno.setApellidos("Lopez");
		alumno.setDni("23987221E");
		
		IDao<Alumno> alumnoDao = new AlumnoDao();
		idAlumno = alumnoDao.add(alumno); 
		
		boolean expectedResult = idAlumno > 0;
		assertTrue(expectedResult);
	}

	@Test
	public void test2GetAll() throws SQLException 
	{
		IDao<Alumno> alumnoDao = new AlumnoDao();
		List<Alumno> alumnos = alumnoDao.getAll();
		
		boolean expectedResult = alumnos.size() >= 1;
		assertTrue(expectedResult);
	}

	@Test
	public void test3GetById() throws SQLException
	{
		IDao<Alumno> alumnoDao = new AlumnoDao();
		Alumno alumno = alumnoDao.getById(idAlumno);
		
		boolean expectedResult = alumno.getIdAlumno() == idAlumno;
		assertTrue(expectedResult);
	}

	@Test
	public void test4Modify() throws SQLException
	{
		Alumno alumno = new Alumno();
		alumno.setNombre("Pepe");
		alumno.setApellidos("Pan i agua");
		alumno.setDni("237429374E");
		alumno.setIdAlumno(idAlumno);
		
		IDao<Alumno> alumnoDao = new AlumnoDao();
		int modifiedRows = alumnoDao.modify(alumno);
		
		boolean expectedResult = modifiedRows == 1;
		assertTrue(expectedResult);
	}

	@Test
	public void test5Remove() throws SQLException {
		IDao<Alumno> alumnoDao = new AlumnoDao();
		int deletedRows = alumnoDao.remove(idAlumno);
		boolean expectedResult = deletedRows == 1;
		assertTrue(expectedResult);
	}

}
