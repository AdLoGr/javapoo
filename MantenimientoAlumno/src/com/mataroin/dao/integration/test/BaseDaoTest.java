package com.mataroin.dao.integration.test;


import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import com.mataroin.dao.AlumnoDao;


public class BaseDaoTest {

	
	@Test
	public void testGetConnection() throws SQLException 
	{
		AlumnoDao dao = new AlumnoDao();
		dao.openConn();
		boolean expectedResult = dao.getConn() != null;
		assertTrue(expectedResult);
	}

	
}
