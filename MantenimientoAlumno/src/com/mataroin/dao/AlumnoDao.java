package com.mataroin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mataroin.model.Alumno;


public class AlumnoDao extends BaseDao implements IDao<Alumno> {
	
	
	private Connection conn = null;
	private PreparedStatement prepStmt = null;

	
	@Override
	public int add(Alumno model) throws SQLException 
	{
		openConn();
		
		String insert = "INSERT INTO alumno(nombre, apellido, dni) VALUES(?, ?, ?)";
		prepStmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
		prepStmt.setString(1, model.getNombre());
		prepStmt.setString(2, model.getApellidos());
		prepStmt.setString(3, model.getDni());
		prepStmt = executeUpdate(prepStmt);
	
		ResultSet rsKeys = prepStmt.getGeneratedKeys();
		rsKeys.next();
		int newKey = rsKeys.getInt(1);
		
		close();
		
		return newKey;
	}
	
	
	@Override
	public Alumno getById(int id) throws SQLException 
	{
		List<Alumno> alumnos = getAll();
		for(Alumno alumno : alumnos)
			if (id == alumno.getIdAlumno())
				return alumno;
		
		throw new IllegalArgumentException(
				String.format("IdAlumno: %d no existe", id));
	}
	
	
	@Override
	public List<Alumno> getAll() throws SQLException 
	{
		openConn();
		
		prepStmt = conn.prepareStatement("SELECT * FROM alumno");
		ResultSet alumnosSet = null;
		alumnosSet = doQuery(prepStmt);
		
		List<Alumno> alumnos = new ArrayList<Alumno>();
		while(alumnosSet.next())
		{
			Alumno alumno = new Alumno();
			alumno.setIdAlumno(alumnosSet.getInt("IdAlumno"));
			alumno.setNombre(alumnosSet.getString("Nombre"));
			alumno.setApellidos(alumnosSet.getString("Apellido"));
			alumno.setDni(alumnosSet.getString("Dni"));
			
			alumnos.add(alumno);
		}
		
		close();
		
		return alumnos;
	}
	
	private ResultSet doQuery(PreparedStatement stmt) throws SQLException
	{
		try
		{
			ResultSet alumnosSet = stmt.executeQuery();
			return alumnosSet;
			
		} catch(SQLException ex) {
			ex.printStackTrace();
			close();
			throw ex;
		}
	}

	
	@Override
	public int modify(Alumno model) throws SQLException 
	{
		openConn();
		
		String update = "UPDATE alumno SET Nombre = ?, Apellido = ?, Dni = ? WHERE idAlumno = ?";
		prepStmt = conn.prepareStatement(update);
		prepStmt.setString(1, model.getNombre());
		prepStmt.setString(2, model.getApellidos());
		prepStmt.setString(3, model.getDni());
		prepStmt.setInt(4, model.getIdAlumno());
		
		int modifiedRows = executeUpdateForInt(prepStmt);
		
		close();
		
		return modifiedRows;
	}

	
	@Override
	public int remove(int id) throws SQLException 
	{
		openConn();
		
		String delete = "DELETE FROM alumno WHERE idAlumno = ?";
		prepStmt = conn.prepareStatement(delete);
		prepStmt.setInt(1, id);
		
		int deletedRows = executeUpdateForInt(prepStmt);
		
		close();
		
		return deletedRows;
	}
	
	
	public void openConn() throws SQLException
	{
		try
		{
			setConn(super.getConnection());
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	private PreparedStatement executeUpdate(PreparedStatement stmt) throws SQLException
	{
		try
		{
			stmt.executeUpdate();
			return stmt;
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			close();
			throw ex;
		}
	}
	
	private int executeUpdateForInt(PreparedStatement stmt) throws SQLException
	{
		try
		{
			int counter = stmt.executeUpdate();
			return counter;
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			close();
			throw ex;
		}
	}
	
	
	private void close() throws SQLException
	{
		closeStmt();
		closeConn();
	}
	
	private void closeConn() throws SQLException
	{
		try
		{
			getConn().close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	private void closeStmt() throws SQLException
	{
		try 
		{
			getPrepStmt().close();
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public PreparedStatement getPrepStmt() {
		return prepStmt;
	}

	public void setPrepStmt(PreparedStatement prepStmt) {
		this.prepStmt = prepStmt;
	}
	
	
	
}
