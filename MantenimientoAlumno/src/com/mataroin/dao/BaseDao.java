package com.mataroin.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class BaseDao {
	
	
	static 
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	
	protected Connection getConnection() throws SQLException
	{
		/*try(Connection connection = 
				DriverManager.getConnection(
						"jdbc:mysql://localhost/academia?user=root");)
		{	
			return connection;
		} catch(SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}*/
		try
		{	
			Connection connection = 
					DriverManager.getConnection(
							"jdbc:mysql://localhost/academia?user=root");
			return connection;
		} catch(SQLException ex) {
			ex.printStackTrace();
			throw ex;
		}
		
	}
	
}
