package com.mataroin.dao.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.collections.ArrayUtils;

import com.mataroin.dao.IDao;
import com.mataroin.model.Alumno;

public class AlumnoMockDaoTest {
	
	
	private IDao<Alumno> mockDao = mock(IDao.class);
	private Alumno alumno;
	
	
	@Before
	public void setUp() throws Exception {
		mockDao = mock(IDao.class);
		alumno = new Alumno("Adria", "Lopez", "3242342E");
	}
	
	
	@Test
	public void thatAddReturnsIntWhenSuccesful() throws Exception {
		int toReturn = 10;
		when(mockDao.add(any(Alumno.class)))
			.thenReturn(toReturn);
		
		int expectedReturn = mockDao.add(alumno);
		assertTrue(expectedReturn == toReturn);
		verify(mockDao).add(alumno);
	}
	
	@Test(expected = SQLException.class) 
	public void thatAddThrowsWhenFails() throws Exception {
		int toReturn = 10;
		when(mockDao.add(any(Alumno.class)))
			.thenThrow(new SQLException());
		
		int expectedReturn = mockDao.add(alumno);
		
		assertFalse(expectedReturn == toReturn);
		verify(mockDao).add(alumno);
	}
	
	
	@Test
	public void thatGetAllReturnsListWhenSuccesful() throws Exception {
		List<Alumno> alumnos = new ArrayList<Alumno>();
		alumnos.add(alumno);
		when(mockDao.getAll())
			.thenReturn(alumnos);
		
		List<Alumno> expectedList = mockDao.getAll();
		
		assertEquals(expectedList, alumnos);
		assertTrue(expectedList.contains(alumno) && alumnos.contains(alumno));
		verify(mockDao).getAll();
	}
	
	@Test
	public void thatGetAllReturnsVoidListWhenVoidDb() throws Exception {
		List<Alumno> alumnos = new ArrayList<Alumno>();
		when(mockDao.getAll())
			.thenReturn(alumnos);
		
		List<Alumno> expectedList = mockDao.getAll();
		
		assertEquals(expectedList, alumnos);
		assertTrue(expectedList.isEmpty() && alumnos.isEmpty());
		verify(mockDao).getAll();
	}
	
	@Test(expected = SQLException.class)  
	public void thatGetAllThrowsExceptionWhenFails() throws Exception {
		when(mockDao.getAll())
			.thenThrow(new SQLException());
		
		List<Alumno> expectedList = mockDao.getAll();
		
		assertTrue(expectedList == null);
		verify(mockDao).getAll();
	}
	
	
	@Test
	public void thatGetByIdReturnsAlumnoWhenSuccesful() throws Exception {
		when(mockDao.getById(anyInt()))
			.thenReturn(alumno);
		
		alumno.setIdAlumno(10);
		Alumno expectedAlumno = mockDao.getById(alumno.getIdAlumno());
		
		assertEquals(expectedAlumno, alumno);
		verify(mockDao).getById(alumno.getIdAlumno());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void thatGetByIdThrowsWhenIdNotExists() throws Exception {
		when(mockDao.getById(anyInt()))
			.thenThrow(new IllegalArgumentException());
		
		alumno.setIdAlumno(10);
		Alumno expectedAlumno = mockDao.getById(alumno.getIdAlumno());
		
		assertTrue(expectedAlumno == null);
		verify(mockDao).getById(alumno.getIdAlumno());
	}
	
	
	@Test
	public void thatModifyReturnsNumberOfModifiedRowsWhenSucessful() throws Exception {
		int modifiedRows = 1;
		when(mockDao.modify(any(Alumno.class)))
			.thenReturn(modifiedRows);
		
		Alumno newAlumno = new Alumno(alumno.getIdAlumno(), "Pepito", "Grillo", 
				"43242342342E");
		int expectedModifiedRows = mockDao.modify(newAlumno);
		
		assertEquals(expectedModifiedRows, modifiedRows);
		verify(mockDao).modify(newAlumno);
	}

	@Test(expected = SQLException.class) 
	public void thatModifyThrowsWhenFailed() throws Exception {
		int modifiedRows = 0;
		when(mockDao.modify(any(Alumno.class)))
			.thenThrow(new SQLException());
		
		Alumno newAlumno = new Alumno(alumno.getIdAlumno(), "Pepito", "Grillo", 
				"43242342342E");
		int expectedModifiedRows = mockDao.modify(newAlumno);
		
		assertTrue(modifiedRows == 0);
		verify(mockDao).modify(newAlumno);
	}
	
	
	@Test
	public void thatRemovesReturnsNumOfRemovedRowsWhenSuccesful() throws Exception {
		int modifiedRows = 1;
		when(mockDao.remove(anyInt()))
			.thenReturn(modifiedRows);
			
		int expectedModifiedRows = mockDao.remove(alumno.getIdAlumno());
		
		assertEquals(expectedModifiedRows, modifiedRows);
		verify(mockDao).remove(alumno.getIdAlumno());
	}
	
	
	@Test(expected = SQLException.class) 
	public void thatRemovesThrowsWhenFailed() throws Exception {
		int modifiedRows = 0;
		when(mockDao.modify(any(Alumno.class)))
			.thenThrow(new SQLException());
		
		Alumno newAlumno = new Alumno(alumno.getIdAlumno(), "Pepito", "Grillo", 
				"43242342342E");
		int expectedModifiedRows = mockDao.modify(newAlumno);
		
		assertTrue(modifiedRows == 0);
		verify(mockDao).modify(newAlumno);
	}
	
	
	
	
	

}
