package com.mataroin.dao;


import java.sql.SQLException;
import java.util.List;


public interface IDao<T> {
	
	
	int add(T model) throws SQLException;
	
	List<T> getAll() throws SQLException;
	T getById(int id) throws SQLException;
	
	int modify(T model) throws SQLException;
	
	int remove(int id) throws SQLException;
	
	
}

