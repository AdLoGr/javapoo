package com.netmind.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MyFirstServlet extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
   
    public MyFirstServlet() {
        super();
    }

    
    protected void doPost(HttpServletRequest request, 
    		HttpServletResponse response) 
    		 throws ServletException, IOException 
    {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) 
			 throws ServletException, IOException 
	{
		response.getWriter().append("Served at: ")
								.append(request.getContextPath());
		response.getWriter().append("\n");
		response.getWriter().append("HELLO WORLD FROM NETMIND");
	}

}
