package com.netmind.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mataroin.business.AlumnoBu;
import com.mataroin.business.IBusiness;
import com.mataroin.model.Alumno;


public class AlumnoController extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
   
    public AlumnoController() {
        super();
    }

    
    protected void doPost(HttpServletRequest request, 
    		HttpServletResponse response) 
    		 throws ServletException, IOException 
    {
			Alumno alumno = new Alumno(request.getParameter("nombre"),
									   request.getParameter("apellido"),
									   request.getParameter("dni"));	
			
			IBusiness<Alumno> alumnoBu = new AlumnoBu();
			try {
				int idAlumno = alumnoBu.add(alumno);
				alumno.setIdAlumno(idAlumno);
				request.setAttribute("alumno", alumno);
				request.getRequestDispatcher("registro.jsp").forward(request, response);
				
			} catch (Exception e) {
				response.getWriter().append("ALUMNO NO HA PODIDO SER A�ADIDO!");
			}
	}

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) 
			 throws ServletException, IOException {
		
		if (request.getParameter("action").equals("delete")) {
    		remove(request, response);
		}
		else if (request.getParameter("action").equals("getAll")) {
			getAll(request, response);
		}
		else if (request.getParameter("action").equals("getById")) {
			getById(request, response);
		}
	}
	
	private void remove(HttpServletRequest request,
			HttpServletResponse response) 
			  throws ServletException, IOException  {
		String idAlumnoToRemoveStr = request.getParameter("idAlumno");
		if (idAlumnoToRemoveStr != null) {
			int idAlumnoToRemove = Integer.parseInt(idAlumnoToRemoveStr);
			IBusiness<Alumno> alumnoBu = new AlumnoBu();
			try {
				
				int removedAlumnosCount = alumnoBu.remove(idAlumnoToRemove);
				if (removedAlumnosCount == 1) {
					request.setAttribute("removedSuccesful", 1);
				}
				
			} catch(Exception ex) {
				request.setAttribute("removedSuccesful", 0);
			}
			request.getRequestDispatcher("deleted.jsp").forward(request, 
					response);
		}
	}
	
	private void getAll(HttpServletRequest request,
			HttpServletResponse response) 
			  throws ServletException, IOException  {	
		IBusiness<Alumno> alumnoBu = new AlumnoBu();
		try {
			
			List<Alumno> alumnos = alumnoBu.getAll();	
			request.setAttribute("alumnos", alumnos);
			request.getRequestDispatcher("view.jsp").forward(request, response);
		
		} catch(Exception ex) {
			throw new ServletException("sql error", ex);
		}		
	}
	
	private void getById(HttpServletRequest request,
			HttpServletResponse response) 
			  throws ServletException, IOException  {	
		
		IBusiness<Alumno> alumnoBu = new AlumnoBu();
		try {
			
			Alumno alumno = alumnoBu.getById(
					Integer.parseInt(request.getParameter("alumnoId")));
			
			request.setAttribute("alumno", alumno);
			request.getRequestDispatcher("viewById.jsp").forward(request, response);
		} catch(Exception ex) {
			throw new ServletException("sql error", ex);
		}
	}
	
	
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String idAlumnoToRemoveStr = req.getParameter("idAlumno");
		if (idAlumnoToRemoveStr != null) {
			int idAlumnoToRemove = Integer.parseInt(idAlumnoToRemoveStr);
			AlumnoBu alumnoBu = new AlumnoBu();
			try {
				
				int removedAlumnosCount = alumnoBu.remove(idAlumnoToRemove);
				if (removedAlumnosCount == 1) {
					req.setAttribute("removedSuccesful", 1);
				}
				
			} catch(Exception ex) {
				req.setAttribute("removedSuccesful", 0);
			}
			req.getRequestDispatcher("deleted.jsp").forward(req, resp);
		}
	}
	
}
