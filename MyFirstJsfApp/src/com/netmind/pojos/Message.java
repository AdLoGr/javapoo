package com.netmind.pojos;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "massage", eager = true)
@RequestScoped
public class Message {
	
	
	private String message = "Hello World!";
	
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
}
