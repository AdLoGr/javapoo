package com.netmind.orientacion.objetos;

import java.util.Scanner;

public class MyScanner {
	
	
	private static Scanner scanner;
	
	
	public static Scanner getScanner()
	{
		if (scanner == null)
			return new Scanner(System.in);
		
		return scanner;
	}
	
}
