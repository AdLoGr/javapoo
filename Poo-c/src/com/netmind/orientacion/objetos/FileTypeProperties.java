package com.netmind.orientacion.objetos;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class FileTypeProperties {
	
	
	private final String propertiesPath = "daoType.properties";
	private static final Logger logger = Logger.getLogger(FileTypeProperties.class);
	
	
	public String getPath(DaoType type) throws IOException
	{
		try 
		{	
			Properties prop = getProperties();
			
			if (type == DaoType.TXT)
			   return prop.getProperty("txtPath");
			if (type == DaoType.JSON)
			   return prop.getProperty("jsonPath");
			
		} catch(IOException ex) {
			throw ex;
		}
		throw new IllegalArgumentException(
				String.format("Illegal argument daoType"));
	}
	
	private Properties getProperties() throws IOException
	{
		try (InputStream inStream = 
				getClass().getClassLoader().getResourceAsStream(propertiesPath))
		{	
			Properties prop = new Properties();
			prop.load(inStream);
			return prop;
		} catch(IOException ex){
			logger.error("Error leyendo properties file", ex);
			throw ex;
		}	
	}
	
}
