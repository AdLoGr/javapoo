package com.netmind.orientacion.objetos;

import java.io.IOException;

public class FileTxt extends AlumnosFile {
	
	
	@Override
	protected String getPath() throws IOException
	{
		return new FileTypeProperties().getPath(DaoType.TXT);
	}
	
	
}
