package com.netmind.orientacion.objetos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DaoTxt implements IDao<Alumno> {

	@Override
	public int add(Alumno alumno) throws Exception {
		try (BufferedWriter buffer = AlumnosFile.getAppendingBuffer())
		{
			buffer.append(alumno.toString());
			buffer.newLine();
			return alumno.getIdAlumno();
		} catch(IOException ex) {
			ex.printStackTrace();
			throw ex;
		}		
	}

	@Override
	public List<Alumno> getAll() throws IOException {
		List<Alumno> alumnos = null;
		try (BufferedReader buffer = AlumnosFile.getBufferedReader())
		{
			alumnos = new ArrayList<>();
			String alumnoEntry = null;
			while ((alumnoEntry = buffer.readLine()) != null) 
			{
				Alumno alumno = Alumno.parse(alumnoEntry);
				alumnos.add(alumno);
			}
		} catch(IOException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return alumnos;
	}

}
