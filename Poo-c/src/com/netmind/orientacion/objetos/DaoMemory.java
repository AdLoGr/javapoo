package com.netmind.orientacion.objetos;

import java.io.IOException;
import java.util.List;

public class DaoMemory implements IDao<Alumno> {
	
	
	private List<Alumno> list;
	
	@Override
	public int add(Alumno alumno) throws Exception 
	{
		if (list != null)
			list = AlumnosList.getInstance();
		
		list.add(alumno);
		return alumno.getIdAlumno();
	}

	@Override
	public List<Alumno> getAll() throws IOException 
	{
		if (list != null)
			return list;
		
		return AlumnosList.getInstance();
	}

	
}
