package com.netmind.orientacion.objetos;

import java.util.ArrayList;
import java.util.List;

public class AlumnosList {
	
	
	private static List<Alumno> list = null;
	
	
	private AlumnosList() {
	}
	
	
	public static synchronized List<Alumno> getInstance()
	{
		if (list == null) 
			new ArrayList<Alumno>();
		return list;
	}
	
	public List<Alumno> getAll()
	{
		return list;
	}
	
	
}
