package com.netmind.orientacion.objetos.tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.netmind.orientacion.objetos.Alumno;
import com.netmind.orientacion.objetos.DaoFile;
import com.netmind.orientacion.objetos.AlumnosFile;

import com.netmind.orientacion.objetos.DaoCreator;
import com.netmind.orientacion.objetos.DaoType;

public class AlumnoDaoTest {
	
	private DaoFile dao = DaoCreator.createFileDao();

	@Test
	public void testAddTxt() throws Exception
	{	
			Alumno alumno = new Alumno("Adria", "Lopez", "1230982E", -10);
			alumno.generateUUID();
			dao.setDaoFileType(DaoType.TXT);
			int addedAlumnoIndex = dao.add(alumno);
			Alumno expectedAlumno = getAlumnoFromFile(addedAlumnoIndex);
			assertTrue(expectedAlumno.hashCode() == alumno.hashCode());
	}
	
	private Alumno getAlumnoFromFile(int id) throws IOException, Exception
	{
		try (BufferedReader buffer = AlumnosFile.getBufferedReader())
		{	
			String alumnoEntry = null;
			while ((alumnoEntry = buffer.readLine()) != null) 
			{
				int alumnoResultId = Alumno.getIdFromAlumnoString(alumnoEntry);
				if (alumnoResultId == id)
				     return Alumno.parse(alumnoEntry);
			}
			
		} catch(IOException ex) {
			ex.printStackTrace();
			throw ex;
		} 
		throw new Exception(
				String.format("Not found alumno with id: %d", id));
	}
	
	
	@Test
	public void testAddJson() throws Exception
	{
			Alumno alumno = new Alumno("Adria", "Lopez", "1230982E", -10);
			alumno.generateUUID();
			dao.setDaoFileType(DaoType.JSON);
			int addedAlumnoIndex = dao.add(alumno);
			Alumno expectedAlumno = getAlumnoFromJson(addedAlumnoIndex);
			assertTrue(expectedAlumno.hashCode() == alumno.hashCode());
	}
	
	private Alumno getAlumnoFromJson(int id) throws IOException, Exception
	{
		try (BufferedReader buffer = AlumnosFile.getBufferedReader())
		{	
			String alumnoEntry = null;
			while ((alumnoEntry = buffer.readLine()) != null) 
			{
				Alumno alumno = Alumno.parseFromJson(alumnoEntry);
				if (alumno.getIdAlumno() == id)
				     return alumno;
			}
			
		} catch(IOException ex) {
			ex.printStackTrace();
			throw ex;
		} 
		throw new Exception(
				String.format("Not found alumno with id: %d", id));
	}
	
	
	@Test
	public void testGetAll() throws Exception
	{
			dao.setDaoFileType(DaoType.TXT);
			List<Alumno> alumnos = dao.getAll();
			assertTrue(alumnos.size() >= 0);
	}
	
	
	@Test
	public void testGetAllJson() throws Exception
	{
			dao.setDaoFileType(DaoType.JSON);
			List<Alumno> alumnos = dao.getAll();
			assertTrue(alumnos.size() >= 0);
	}
	
}
