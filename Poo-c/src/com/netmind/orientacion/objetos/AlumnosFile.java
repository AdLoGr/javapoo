package com.netmind.orientacion.objetos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

public abstract class AlumnosFile {
	
	
	private static AlumnosFile alumnoFile = new FileTxt();
	private static Logger logger = Logger.getLogger(AlumnosFile.class);
	
	
	protected abstract String getPath() throws IOException;
	
	
	public static File getFile() throws IOException
	{
		try
		{
			File file = new File(alumnoFile.getPath());
			if (!file.exists())
				createFile(file);
			return file;
			
		} catch(IOException ex) {
			logger.error(ex);
			throw ex;
		}
	}
	
	public static BufferedReader getBufferedReader() throws IOException
	{
		try
		{
			return new BufferedReader(new FileReader(getFile()));
			
		} catch(IOException ex) {
			logger.error(ex);
			throw ex;
		}			  
	}
	
	public static BufferedWriter getAppendingBuffer() throws IOException
	{
		try
		{
			return new BufferedWriter(
					new FileWriter(getFile(), true));
			
		} catch(IOException ex) {
			logger.error(ex);
			throw ex;
		}	
	}
	
	public static FileWriter getAppendingWriter() throws IOException
	{
		try
		{
			return new FileWriter(getFile(), true);
			
		} catch(IOException ex) {
			logger.error(ex);
			throw ex;
		}	
	}
	
	public static void setFileType(AlumnosFile alumnosFile)
	{
		alumnoFile = alumnosFile;
	}
	
	
	private static void createFile(File file) 
	{
		try 
		{
			file.createNewFile();
			
		} catch(IOException ex) {
			logger.error(ex);
		}
	}
	
	
}
