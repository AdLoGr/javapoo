package com.netmind.orientacion.objetos;

import java.util.List;

public interface IDao<T> {
	abstract int add(T objeto) throws Exception;
	abstract List<T> getAll() throws Exception;
}
