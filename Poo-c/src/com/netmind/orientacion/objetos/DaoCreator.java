package com.netmind.orientacion.objetos;

public class DaoCreator {

	
	public static IDao<Alumno> createMemoryDao() 
	{
		return new DaoMemory();
	}
	
	public static DaoFile createFileDao() 
	{
		return DaoFile.getInstance();
	}
	
}
