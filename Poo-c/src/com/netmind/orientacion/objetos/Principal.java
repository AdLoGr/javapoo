package com.netmind.orientacion.objetos;

import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class Principal {
	
	
	final static Logger logger = Logger.getLogger(Principal.class);
	final static IDao<Alumno> dao = DaoCreator.createMemoryDao();
	
	
	public static void main(String[] args) 
	{	

		Scanner scan = MyScanner.getScanner();
		
		boolean keepRunning = false;
		while (!keepRunning)
		{
			showMenu();	
			int action = getUserAction();
			switch(action)
			{
			case 1: 
				insertAlumno();
				break;
			case 2: 
				printAllAlumnos();
				break;
			case 3: 
				//changeFileFormat();
				break;
			default:
				System.exit(0);
				break;
			}
		}
		
		scan.close();
	}
	
	public static void showMenu()
	{
		System.out.println("\nMENU");
		System.out.println("\n1-Insertar nuevo alumno");
		System.out.println("\n2-Ver todos los alumnos");
		System.out.println("\n3-Cambiar file type");
		System.out.println("\n0-Salir");
	}
	
	public static int getUserAction() 
	{
		Scanner scan = MyScanner.getScanner();
		int action = Integer.parseInt(scan.nextLine());
		return action;
	}
	
	public static void insertAlumno() 
	{
		try 
		{
			dao.add(Alumno.getAlumnoInput());
		} catch (Exception e) {
			System.out.println("Error a�adiendo alumno!");
			logger.error("ERROR", e);
		}
	}
	
	public static void printAllAlumnos() 
	{
		try 
		{
			List<Alumno> alumnos = dao.getAll();
			for(Alumno alumno : alumnos)
				System.out.println(alumno.toString());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error mostrando!");
		}
	}
	
	/*public static void changeFileFormat()
	{
		Scanner scan = MyScanner.getScanner();
		System.out.println("1- TXT");
		System.out.println("2- JSON");
		int action = Integer.parseInt(scan.nextLine());
		if (action == 1)
			DaoSelector.selectDaoType(DaoType.TXT);
		else
			DaoSelector.selectDaoType(DaoType.JSON);
	}*/
	
}
