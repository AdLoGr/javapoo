package com.netmind.orientacion.objetos;


import java.util.List;

public class DaoFile implements IDao<Alumno> {
	
	
	private IDao<Alumno> dao;
	private DaoType currentType;
	
	
	protected DaoFile(){
		dao = new DaoTxt();
		currentType = DaoType.TXT;
		AlumnosFile.setFileType(new FileTxt());
	}	
	
	
	public static DaoFile getInstance()
	{
		return new DaoFile();
	}
	
	public int add(Alumno alumno) throws Exception
	{
		return dao.add(alumno);
	}
	
	public List<Alumno> getAll() throws Exception
	{
		return dao.getAll();
	}
	
	public void setDaoFileType(DaoType type) 
	{
		if (type == currentType)
			return;
			
		if (type == DaoType.TXT )
		{
			dao = new DaoTxt();
			AlumnosFile.setFileType(new FileTxt());
		}
		if (type == DaoType.JSON)
		{
			dao = new DaoJson();
			AlumnosFile.setFileType(new FileJson());
		}
		
		currentType = type;
	}
	
	public IDao<Alumno> getCurrentDao()
	{
		return dao;
	}


}
