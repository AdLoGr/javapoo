package com.netmind.calculadora.integration.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.netmind.calculadora.Calculadora;

public class CalculadoraTest {

	@Test
	public void testSuma()
	{
		Calculadora calculator = new Calculadora();
		int result = calculator.suma(2, 3);
		int expectedResult = 5;
		assertTrue(result == expectedResult);
	}

	@Test
	public void testResta() 
	{
		Calculadora calculator = new Calculadora();
		int result = calculator.resta(2, 3);
		int expectedResult = -1;
		assertTrue(result == expectedResult);
	}

	@Test
	public void testMultiplicacion() 
	{
		Calculadora calculator = new Calculadora();
		int result = calculator.multiplicacion(2, 3);
		int expectedResult = 6;
		assertTrue(result == expectedResult);
	}

	@Test
	public void testDivision() 
	{
		Calculadora calculator = new Calculadora();
		int result = calculator.division(10, 5);
		int expectedResult = 2;
		assertTrue(result == expectedResult);
	}

}
