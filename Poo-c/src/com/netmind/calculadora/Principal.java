package com.netmind.calculadora;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args)
	{
		System.out.println("Numbers: ");
		Scanner scan = new Scanner(System.in);
		int firstNumber = scan.nextInt();
		int secondNumber = scan.nextInt();
		scan.close();
		
		Calculadora calculadora = new Calculadora();
		int resultSuma = calculadora.suma(firstNumber, secondNumber);
		int resultResta = calculadora.resta(firstNumber, secondNumber);
		int resultMultiplicacion = 
				calculadora.multiplicacion(firstNumber, secondNumber);
		int resultDivision = calculadora.division(firstNumber, secondNumber);
		
		StringBuilder resultadosString = new StringBuilder();
		resultadosString.append("\nSuma:" ).append(resultSuma);
		resultadosString.append("\nResta: " ).append(resultResta);
		resultadosString.append("\nMulti: " ).append(resultMultiplicacion);
		resultadosString.append("\nDivision: " ).append(resultDivision);
		System.out.println(resultadosString.toString());
	}

}
