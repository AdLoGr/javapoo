package com.netmind.orientacion.objetos;

import java.io.File;

public class FileJson extends AlumnosFile {
	
	
	private String path;
	
	
	public FileJson() 
	{
		this.path = "C:\\Users\\a5alumno\\workspace\\Poo\\build\\prueva.json";
	}
	
	public FileJson(String path) 
	{
		this.path = path;
	}
	

	@Override
	protected File _getFile() 
	{
		File file = new File(path);
		if (!file.exists())
			createFile(file);
		return file;
	}

}
