package com.netmind.orientacion.objetos;

import java.io.File;

public class FileTxt extends AlumnosFile {
	
	
	private String path;
	
	
	public FileTxt() 
	{
		this.path = "C:\\Users\\a5alumno\\workspace\\Poo\\build\\prueva.txt";
	}
	
	public FileTxt(String path) 
	{
		this.path = path;
	}

	
	@Override
	protected File _getFile() 
	{
		File file = new File(path);
		if (!file.exists())
			createFile(file);
		return file;
	}
	
	
}
