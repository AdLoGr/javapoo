package com.netmind.orientacion.objetos;

public class DaoSelector {
	
	public static void selectDaoType(DaoType type)
	{
		if (type == DaoType.TXT)
		{
			AlumnoDao.setFileType(new DaoTxt());
			AlumnosFile.setFileType(new FileTxt());
		}
		else if (type == DaoType.JSON)
		{
			AlumnoDao.setFileType(new DaoJson());
			AlumnosFile.setFileType(new FileJson());
		}
	}

}
