package com.netmind.orientacion.objetos;


import java.io.IOException;
import java.util.List;

public abstract class AlumnoDao {
	
	
	private static AlumnoDao dao = new DaoTxt();

	
	protected abstract int _add(Alumno alumno) throws Exception;
	protected abstract List<Alumno> _getAll() throws IOException;
	
	
	public static int add(Alumno alumno) throws Exception
	{
		return dao._add(alumno);
	}
	
	public static List<Alumno> getAll() throws Exception
	{
		return dao._getAll();
	}
	
	public static void setFileType(AlumnoDao newDaoType) 
	{
		dao = newDaoType;
	}
	
}
