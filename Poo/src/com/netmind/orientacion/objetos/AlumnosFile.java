package com.netmind.orientacion.objetos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public abstract class AlumnosFile {
	
	
	private static AlumnosFile alumnoFile = new FileTxt();
	
	
	protected abstract File _getFile();
	
	
	public static File getFile() 
	{
		return alumnoFile._getFile();
	}
	
	public static BufferedReader getBufferedReader() throws FileNotFoundException
	{
		try
		{
			return new BufferedReader(new FileReader(getFile()));
		} catch(FileNotFoundException ex) {
			ex.printStackTrace();
			throw ex;
		}			  
	}
	
	public static BufferedWriter getAppendingBuffer() throws IOException
	{
		try
		{
			return new BufferedWriter(
					new FileWriter(getFile(), true));
		} catch(IOException ex) {
			throw ex;
		}	
	}
	
	public static FileWriter getAppendingWriter() throws IOException
	{
		try
		{
			return new FileWriter(getFile(), true);
		} catch(IOException ex) {
			throw ex;
		}	
	}
	
	public static void setFileType(AlumnosFile alumnosFile)
	{
		alumnoFile = alumnosFile;
	}
	
	
	protected void createFile(File file) 
	{
		try 
		{
			file.createNewFile();
		} catch(IOException ex) {
			System.out.println("Error al crear el archivo!");
			System.out.println(ex.toString());
		}
	}
	
	
}
