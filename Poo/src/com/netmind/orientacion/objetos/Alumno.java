package com.netmind.orientacion.objetos;

import java.util.Scanner;
import java.util.UUID;

import com.google.gson.Gson;

public class Alumno extends NetmindObject {
	
	private int idAlumno;
	private String nombre;
	private String apellidos;
	private String dni;
	
	
	public Alumno(){
	}
	
	public Alumno(String nombre, String apellidos, String dni, int idAlumno) {
		super();
		this.idAlumno = idAlumno;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
	}
	
	public Alumno(String nombre, String apellidos, String dni, int idAlumno, UUID uuid) {
		super();
		this.idAlumno = idAlumno;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		setUuid(uuid);
	}
	

	public static Alumno getAlumnoInput() 
	{
		System.out.println("-- NUEVO ALUMNO --");
		System.out.println("Escribe nombre: ");
		Scanner scan = MyScanner.getScanner();
		String nombre = scan.nextLine();
		System.out.println("Escribe apellido: ");
		String apellidos = scan.nextLine();
		System.out.println("Escribe dni: ");
		String dni = scan.nextLine();
		System.out.println("Escribe ID: ");
		int idAlumno = Integer.parseInt(scan.nextLine());
		
		Alumno alumno = new Alumno();
		alumno.setNombre(nombre);
		alumno.setApellidos(apellidos);
		alumno.setDni(dni);
		alumno.setIdAlumno(idAlumno);
		alumno.generateUUID();
		return alumno;
	}
	
	public static int getIdFromAlumnoString(String alumno) 
	{
		String[] campos = alumno.split(",");	
		return Integer.parseInt(campos[3]);
	}
	
	
	public static Alumno parse(String alumno) 
	{
		String[] campos = alumno.split(",");
		Alumno alumn = new Alumno(campos[0], campos[1],campos[2], 
				 Integer.parseInt(campos[3]));
		
		if (campos.length == 5)
			alumn.setUuid(UUID.fromString(campos[4]));
		
		return alumn;
	}
	
	public static Alumno parseFromJson(String alumno) 
	{
		Gson gson = new Gson();
		Alumno alumn = gson.fromJson(alumno, Alumno.class);
		return alumn;
	}
	
	public int getIdAlumno() 
	{
		return idAlumno;
	}
	
	public void setIdAlumno(int idAlumno) 
	{
		this.idAlumno = idAlumno;
	}
	
	public String getNombre() 
	{
		return nombre;
	}
	
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}
	
	public String getApellidos() 
	{
		return apellidos;
	}
	
	public void setApellidos(String apellidos) 
	{
		this.apellidos = apellidos;
	}
	
	public String getDni() 
	{
		return dni;
	}
	
	public void setDni(String dni)
	{
		this.dni = dni;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((apellidos == null) ? 0 : apellidos.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + idAlumno;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumno other = (Alumno) obj;
		if (apellidos == null) {
			if (other.apellidos != null)
				return false;
		} else if (!apellidos.equals(other.apellidos))
			return false;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (idAlumno != other.idAlumno)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() 
	{
		StringBuilder alumno = new StringBuilder();
		alumno.append(getNombre()).append(",")
				.append(getApellidos()).append(",")
					.append(getDni()).append(",")
						.append(getIdAlumno());
		
		if (getUuid() != null)
			alumno.append(",").append(getUuid().toString());

		return alumno.toString();
	}
	
}
