package com.netmind.orientacion.objetos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class DaoJson extends AlumnoDao {

	@Override
	protected int _add(Alumno alumno) throws Exception {
		try (Writer writer = AlumnosFile.getAppendingWriter();
				BufferedWriter buffered = new BufferedWriter(writer))
		{
			Gson gson = new Gson();
			gson.toJson(alumno, writer);
			buffered.newLine();
			return alumno.getIdAlumno();
		} catch(IOException ex) {
			ex.printStackTrace();
			throw ex;
		}	
	}

	@Override
	protected List<Alumno> _getAll() throws IOException {
		List<Alumno> alumnos = null;
		try (BufferedReader buffer = AlumnosFile.getBufferedReader())
		{
			alumnos = new ArrayList<>();
			String alumnoEntry = null;
			while ((alumnoEntry = buffer.readLine()) != null) 
			{
				Alumno alumno = Alumno.parseFromJson(alumnoEntry);
				alumnos.add(alumno);
			}
		} catch(IOException ex) {
			ex.printStackTrace();
			throw ex;
		}
		return alumnos;
	}
	
	

}
