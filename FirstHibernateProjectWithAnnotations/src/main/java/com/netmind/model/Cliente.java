package com.netmind.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private int idCliente;
	private String nombre;
	private String apellido;
	private String dni;
	private Direccion direccion;

	
	public Cliente(){	
	}
	
	public Cliente(int idCliente, String nombre, String apellido, String dni, Direccion direccion) {
		super();
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.direccion = direccion;
	}
	
	public Cliente(String nombre, String apellido, String dni, Direccion direccion) {
		this(0, nombre, apellido, dni, direccion);
	}
	
	public Cliente(int idCliente, String nombre, String apellido, String dni) {
		this(idCliente, nombre, apellido, dni, null);
	}
	
	public Cliente(String nombre, String apellido, String dni) {
		this(0, nombre, apellido, dni, null);
	}
	
	public Cliente(Cliente cliente) {
		this(cliente.getIdCliente(), cliente.getNombre(), cliente.getApellido(),
				cliente.getDni());
		this.setDireccion(new Direccion(cliente.getDireccion()));
	}
	
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IdCliente")
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	

	@Column(name = "Nombre", nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	@Column(name = "Apellido", nullable = false)
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	
	@Column(name = "Dni", nullable = false)
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}

	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL)
	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	
	public void setAll(String nombre, String apellido, String dni) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}

	@Override
	public String toString() {
		return "Cliente [idCliente=" + idCliente + ", nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni
				+ "]";
	}

}
