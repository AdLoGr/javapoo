package com.netmind.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;


@Entity
@Table(name = "direccion")
public class Direccion implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private int clienteId;
	private Cliente cliente;
	private String pueblo;
	private String provincia;
	private String pais;
	
	
	public Direccion() {
	}
	
	public Direccion(int clienteId, Cliente cliente, String pueblo, String provincia, String pais) {
		super();
		this.clienteId = clienteId;
		this.cliente = cliente;
		this.pueblo = pueblo;
		this.provincia = provincia;
		this.pais = pais;
	}
	
	public Direccion(Cliente cliente, String pueblo, String provincia, String pais) {
		this(0, cliente, pueblo, provincia, pais);
	}
	
	public Direccion(Direccion direccion) {
		this(direccion.getClienteId(), direccion.getCliente(),
				direccion.getPueblo(), direccion.getProvincia(), 
					direccion.getPais());
	}

	
	@GenericGenerator(name = "generator", strategy = "foreign", 
			parameters = @Parameter(name = "property", value = "cliente"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "clienteId", unique = true, nullable = false)
	public int getClienteId() {
		return clienteId;
	}
	
	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Column(name = "pueblo", nullable = false)
	public String getPueblo() {
		return pueblo;
	}
	
	public void setPueblo(String pueblo) {
		this.pueblo = pueblo;
	}
	
	@Column(name = "provincia", nullable = false)
	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	@Column(name = "pais", nullable = false)
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Direccion [clienteId=" + clienteId + ", pueblo=" + pueblo + ", provincia="
				+ provincia + ", pais=" + pais + "]";
	}
	
	

}
