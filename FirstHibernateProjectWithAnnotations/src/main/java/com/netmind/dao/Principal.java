package com.netmind.dao;

import org.hibernate.Transaction;

import com.netmind.model.Cliente;
import com.netmind.model.Direccion;

public class Principal {

	public static void main(String[] args) throws Exception {
		
		Cliente client = new Cliente("adria", "lopez", "231231e");
		Direccion direccion = new Direccion(client, "barcelona", "barcelona", "espa�a");
		client.setDireccion(direccion);
		
		AutoCloseableSession ses = new AutoCloseableSession(
				HibernateUtil2.getSessionFactory().openSession());
		
		Transaction trans = ses.delegate().getTransaction();
		trans.begin();
		ses.delegate().save(client);
		trans.commit();
		ses.close();
	}

}
