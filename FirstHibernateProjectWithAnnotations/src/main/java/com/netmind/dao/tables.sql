CREATE TABLE `direccion` (
  `clienteId` int(11) NOT NULL,
  `pueblo` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  PRIMARY KEY (`cliente_id`),
  CONSTRAINT `FK_CLIENTE_ID` FOREIGN KEY (`clienteId`) REFERENCES `cliente`(`IdCliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `cliente` (
  `IdCliente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(50) NOT NULL,
  `Dni` varchar(50) NOT NULL,
  PRIMARY KEY (`IdCliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;