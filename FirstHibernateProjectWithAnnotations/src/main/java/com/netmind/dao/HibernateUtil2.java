package com.netmind.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.netmind.model.Cliente;
import com.netmind.model.Direccion;

public class HibernateUtil2 {
	
	private static final SessionFactory sessionFactory;
	
	static {
		sessionFactory = new Configuration()
				.addAnnotatedClass(Cliente.class)
				.addAnnotatedClass(Direccion.class)
					.configure().buildSessionFactory();
	}

	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
