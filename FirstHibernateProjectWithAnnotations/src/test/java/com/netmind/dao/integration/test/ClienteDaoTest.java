package com.netmind.dao.integration.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.netmind.dao.ClienteDao;
import com.netmind.model.Cliente;
import com.netmind.model.Direccion;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClienteDaoTest {
	
	
	ClienteDao dao;
	Cliente currentClient;
	Cliente addedClient;
	Direccion direccion;
	
	
	@Before
	public void setUp() throws Exception {
	}
	
	
	@Test
	public void test1AddReturnsValidIdWhenSuccesful() {
		currentClient = new Cliente("Adria", "Lopez", "324234E");
		direccion = new Direccion(currentClient, "barcelona", "barcelona", "espa�a");
		currentClient.setDireccion(direccion);
		
		dao = new ClienteDao();
		addedClient = dao.add(currentClient);
		
		assertTrue(addedClient.getIdCliente() > 0);
		assertTrue(addedClient.getDireccion().getClienteId() >= 0);
		assertTrue(addedClient.getIdCliente() == addedClient.getDireccion().getClienteId());
	}
	
	@Test
	public void test2GetAllReturnsOneClient() {
		dao = new ClienteDao();
		List<Cliente> clientes = dao.getAll();
		
		assertTrue(clientes.size() >= 1);
		assertTrue(clientes.get(0).getDireccion().equals(direccion));
	}
	
	@Test
	public void test3ModifyReturnsModifiedClient() {
		dao = new ClienteDao();
		Cliente returnedCliente = dao.modify(addedClient);
		
		assertTrue(addedClient.equals(returnedCliente));
	}
	
	@Test
	public void test4RemoveRemovesClient() {
		 dao = new ClienteDao();
		 int idRemovedClient = dao.remove(addedClient.getIdCliente());
		
		 assertTrue(idRemovedClient == currentClient.getIdCliente());
	}

}
