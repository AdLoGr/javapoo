package com.netmind.dao.integration.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.netmind.dao.ClienteDao;
import com.netmind.model.Cliente;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClienteDaoTest {
	
	
	ClienteDao dao;
	Cliente currentClient;
	
	
	@Test
	public void test1AddReturnsValidIdWhenSuccesful() {
		currentClient = new Cliente("Adria", "Lopez", "324234E");
		dao = new ClienteDao();
		dao.add(currentClient);
		
		assertTrue(currentClient.getIdCliente() > 0);
	}
	
	@Test
	public void test2GetAllReturnsOneClient() {
		dao = new ClienteDao();
		List<Cliente> clientes = dao.getAll();
		
		assertTrue(clientes.size() == 1);
	}
	
	@Test
	public void test3ModifyReturnsModifiedClient() {
		currentClient.setAll("Pepe", "Grillo", "232123123E");
		dao = new ClienteDao();
		Cliente returnedCliente = dao.modify(currentClient);
		
		assertTrue(currentClient.equalsWithoutId(returnedCliente));
	}
	
	@Test
	public void test4RemoveRemovesClient() {
		 dao = new ClienteDao();
		 int idRemovedClient = dao.remove(currentClient.getIdCliente());
		
		 assertTrue(idRemovedClient == currentClient.getIdCliente());
	}

}
