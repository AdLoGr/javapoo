package com.netmind.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.netmind.model.Cliente;

public class ClienteDao {
	
	
	Session session = null;
	
	
	public ClienteDao() {
		session = HibernateUtil2.getSessionFactory().openSession();
	}
	
	
	public Cliente add(Cliente cliente) throws HibernateException {
		try (AutoCloseableSession sess = new AutoCloseableSession(session)) {
			Transaction transaction = sess.delegate().beginTransaction();
			sess.delegate().save(cliente);
			transaction.commit();
			return cliente;
				
		} catch(Exception ex) {
			throw new HibernateException(ex);
		}
	}
	
	public List<Cliente> getAll() throws HibernateException {
		try (AutoCloseableSession sess = new AutoCloseableSession(session)) {
			Transaction transaction = sess.delegate().beginTransaction();
			Query getAllQuery = sess.delegate().createQuery("FROM Cliente");
			List<Cliente> clientes = (List<Cliente>)getAllQuery.list();
			transaction.commit();
			return clientes;
				
		} catch(Exception ex) {
			throw new HibernateException(ex);
		}
	}
	
	public Cliente modify(Cliente cliente) {
		try (AutoCloseableSession sess = new AutoCloseableSession(session)) {
			Transaction transaction = sess.delegate().beginTransaction();
			Cliente clienteDB = (Cliente)sess.delegate().get(Cliente.class 
					,cliente.getIdCliente());
			clienteDB.setAll(cliente.getNombre(), cliente.getApellido()
					,cliente.getDni());
			transaction.commit();
			return clienteDB;
				
		} catch(Exception ex) {
			throw new HibernateException(ex);
		}
	}
	
	public int remove(int idCliente) {
		try (AutoCloseableSession sess = new AutoCloseableSession(session)) {
			Transaction transaction = sess.delegate().beginTransaction();
			Cliente clienteDB = (Cliente)sess.delegate().get(Cliente.class
					,idCliente);
			sess.delegate().delete(clienteDB);
			transaction.commit();
			return clienteDB.getIdCliente();
				
		} catch(Exception ex) {
			throw new HibernateException(ex);
		}
	}
	

}
