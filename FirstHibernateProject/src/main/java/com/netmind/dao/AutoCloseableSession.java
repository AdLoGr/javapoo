package com.netmind.dao;

import org.hibernate.Session;

public class AutoCloseableSession implements AutoCloseable {

	
	private Session session;
	
	
	public AutoCloseableSession(Session session) {
		setSession(session);
	}
	
	
	public void close() throws Exception {
		session.close();
	}

	public Session delegate() {
		return session;
	}
	
	private void setSession(Session session) {
		this.session = session;
	}
	
}
