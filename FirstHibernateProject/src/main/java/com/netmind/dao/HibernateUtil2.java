package com.netmind.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil2 {
	
	private static final SessionFactory sessionFactory;
	
	static {
		sessionFactory = new Configuration().configure().buildSessionFactory();
	}

	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
