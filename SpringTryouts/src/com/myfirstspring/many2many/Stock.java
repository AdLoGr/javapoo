package com.myfirstspring.many2many;

import java.util.Set;

public class Stock {

	
	private int idStock;
	private String code;
	private String name;
	private Set<Category> categories;
	
	
	public Stock() {
	}
	
	public Stock(int idStock, String code, String name) {
		super();
		this.idStock = idStock;
		this.code = code;
		this.name = name;
	}
	
	public Stock(int idStock, String code, String name, Set<Category> categories) {
		super();
		this.idStock = idStock;
		this.code = code;
		this.name = name;
		this.categories = categories;
	}

	public int getIdStock() {
		return idStock;
	}

	public void setIdStock(int idStock) {
		this.idStock = idStock;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	
	
}
