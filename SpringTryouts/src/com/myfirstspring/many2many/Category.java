package com.myfirstspring.many2many;

import java.util.Set;

public class Category {
	
	
	private int idCategory;
	private String name;
	private String description;
	private Set<Stock> stocks;
	
	
	public Category() {
	}
	

	public Category(int idCategory, String name, String description) {
		super();
		this.idCategory = idCategory;
		this.name = name;
		this.description = description;
	}
	
	public Category(int idCategory, String name, String description, Set<Stock> stocks) {
		super();
		this.idCategory = idCategory;
		this.name = name;
		this.description = description;
		this.stocks = stocks;
	}


	public int getIdCategory() {
		return idCategory;
	}


	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Set<Stock> getStocks() {
		return stocks;
	}


	public void setStocks(Set<Stock> stocks) {
		this.stocks = stocks;
	}

}
