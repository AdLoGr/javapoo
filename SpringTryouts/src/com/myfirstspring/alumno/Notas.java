package com.myfirstspring.alumno;

import java.util.List;

public class Notas {
	
	
	List<String> notas;
	
	
	public Notas() {		
	}

	public Notas(List<String> notas) {
		super();
		this.notas = notas;
	}
	
	
	public void add(String nota) {
		getNotas().add(nota);
	}
	
	public String getNota(int id) {
		return getNotas().get(id);
	}

	public List<String> getNotas() {
		return notas;
	}

	public void setNotas(List<String> notas) {
		this.notas = notas;
	}

}
