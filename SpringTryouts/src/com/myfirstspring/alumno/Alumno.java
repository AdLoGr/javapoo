package com.myfirstspring.alumno;

import java.util.List;

import com.myfirstspring.direccion.Direccion;

public class Alumno {
	
	
	private int idAlumno;
	private String nombre;
	private String apellidos;
	private String dni;
	private Direccion direccion;
	private List<Nota> notas;
	
	
	public Alumno() {
		
	}
	
	public Alumno(int idAlumno, String nombre, String apellidos, String dni, 
			Direccion direccion, List<Nota> notas) {
		super();
		setIdAlumno(idAlumno);
		setNombre(nombre);
		setApellidos(apellidos);
		setDni(dni);
		setDireccion(direccion);
		setNotas(notas);
	}

	
	public int getIdAlumno() {
		return idAlumno;
	}
	
	public void setIdAlumno(int idAlumno) {
		this.idAlumno = idAlumno;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
		direccion.setAlumno(this);
	}
	
	public List<Nota> getNotas() {
		return notas;
	}

	public void setNotas(List<Nota> notas) {
		this.notas = notas;
	}

	public String getNota(int id) {
		return notas.get(id).getText();
	}
	
	
	@Override
	public String toString() {
		return "Alumno [idAlumno=" + idAlumno + ", nombre=" + nombre + ", apellidos=" + apellidos + ", dni=" + dni
				+ ", direccion=" + direccion.getCalle() + "]";
	}
	


}
