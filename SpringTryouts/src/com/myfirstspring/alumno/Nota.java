package com.myfirstspring.alumno;

public class Nota {
	
	private int id;
	private String text;
	
	
	public Nota() {
	}
	
	public Nota(int id, String nota) {
		super();
		this.id = id;
		this.text = nota;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String nota) {
		this.text = nota;
	}
	
}
