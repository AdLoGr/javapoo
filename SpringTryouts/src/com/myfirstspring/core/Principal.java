package com.myfirstspring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myfirstspring.alumno.Alumno;
import com.myfirstspring.direccion.Direccion;
import com.myfirstspring.many2many.Category;
import com.myfirstspring.many2many.Stock;

public class Principal {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"SpringBeans.xml");
		
		/*HelloWorld helloWorld = (HelloWorld)context.getBean("helloWorld");
		helloWorld.printHello();
		
		HelloWorld helloWorldConst = 
				(HelloWorld)context.getBean("helloWorldConst");
		helloWorldConst.printHello();*/
		
		Alumno alumno = (Alumno)context.getBean("alumno");
		Direccion direccion = (Direccion)context.getBean("direccion");
		
		System.out.println(alumno.toString());
		System.out.println(direccion.toString());
		System.out.println(alumno.getNota(1));
		
		
		Alumno alumnoc = (Alumno)context.getBean("alumnoC");
		Direccion direccionc = (Direccion)context.getBean("direccionC");
		
		System.out.println(alumnoc.toString());
		System.out.println(direccionc.toString());
		System.out.println(alumnoc.getNota(0));
		
		System.out.println("\nMANY 2 MANY STOCK");
		Stock stock = (Stock)context.getBean("stock");
		for (Category category : stock.getCategories()) {
			System.out.println(category.getName());
		}
		
		System.out.println("\nMANY 2 MANY STOCK ");
		Stock stock2 = (Stock)context.getBean("stock2");
		for (Category category : stock2.getCategories()) {
			System.out.println(category.getName());
		}
		
		System.out.println("\nMANY 2 MANY CATEGORIES");
		Category categori = (Category)context.getBean("category");
		for (Stock stock1 : categori.getStocks()) {
			System.out.println(stock1.getName());
		}
		
		System.out.println("\nMANY 2 MANY CATEGORIES");
		Category categori2 = (Category)context.getBean("category");
		for (Stock stock1 : categori2.getStocks()) {
			System.out.println(stock1.getName());
		}
		
		((ClassPathXmlApplicationContext)context).close();
	}
	
}
