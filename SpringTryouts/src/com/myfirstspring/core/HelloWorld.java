package com.myfirstspring.core;

public class HelloWorld {
	
	
	private String name;
	
	
	public HelloWorld(){		
	}
	
	public HelloWorld(String name) {
		setName(name);
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	public void printHello() {
		System.out.println(String.format("Spring 4.2.5. : Hello! %s", getName()));
	}

}
