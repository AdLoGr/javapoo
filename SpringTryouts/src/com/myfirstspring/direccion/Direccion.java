package com.myfirstspring.direccion;

import com.myfirstspring.alumno.Alumno;

public class Direccion {
	
	
	private int idDireccion;
	private String calle;
	private String poblacion;
	private String provincia;
	private String cp;
	private Alumno alumno;
	
	
	public Direccion() {
		
	}
	
	public Direccion(int idDireccion, String calle, String poblacion, 
			String provincia, String cp) {
		super();
		setIdDireccion(idDireccion);
		setCalle(calle);
		setPoblacion(poblacion);
		setProvincia(provincia);
		setCp(cp);
	}
	

	public int getIdDireccion() {
		return idDireccion;
	}
	
	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}
	
	public String getCalle() {
		return calle;
	}
	
	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	public String getPoblacion() {
		return poblacion;
	}
	
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
	
	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	public String getCp() {
		return cp;
	}
	
	public void setCp(String cp) {
		this.cp = cp;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		if (getAlumno() != alumno) 
			this.alumno = alumno;
	}

	@Override
	public String toString() {
		return "Direccion [idDireccion=" + idDireccion + ", calle=" + calle + ", poblacion=" + poblacion
				+ ", provincia=" + provincia + ", cp=" + cp + ", alumno=" + alumno.getNombre() + "]";
	}
	
	
	

}
