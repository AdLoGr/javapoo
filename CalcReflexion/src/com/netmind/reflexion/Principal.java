package com.netmind.reflexion;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.ServiceConfigurationError;

public class Principal {
	

	public static void main(String[] args) throws ClassNotFoundException
	{
		System.out.println("Numbers: ");
		Scanner scan = new Scanner(System.in);
		int firstNumber = scan.nextInt();
		int secondNumber = scan.nextInt();
		scan.close();
		
		try
		{
			Class<?> calculadoraClass = Class.forName("com.netmind.reflexion.Calculadora");
			Constructor<?> constructor = calculadoraClass.getConstructor(
											new Class[]{int.class, int.class});
			Calculadora calculadora = (Calculadora)constructor.newInstance(firstNumber, secondNumber);
			
			int resultSuma = calculadora.suma(firstNumber, secondNumber);
			int resultResta = calculadora.resta(firstNumber, secondNumber);
			int resultMultiplicacion = 
					calculadora.multiplicacion(firstNumber, secondNumber);
			int resultDivision = calculadora.division(firstNumber, secondNumber);
			
			StringBuilder resultadosString = new StringBuilder();
			resultadosString.append("\nSuma:" ).append(resultSuma);
			resultadosString.append("\nResta: " ).append(resultResta);
			resultadosString.append("\nMulti: " ).append(resultMultiplicacion);
			resultadosString.append("\nDivision: " ).append(resultDivision);
			System.out.println("REFLEXION CONSTRUCT!");
			System.out.println(resultadosString.toString());
			
			
			Method method = calculadoraClass.getMethod("suma", int.class, int.class);
			resultSuma = (int)method.invoke(calculadora, firstNumber, secondNumber);
			
			method = calculadoraClass.getMethod("resta", int.class, int.class);
			resultResta = (int)method.invoke(calculadora, firstNumber, secondNumber);
			
			method = calculadoraClass.getMethod("multiplicacion", int.class, int.class);
			resultMultiplicacion = (int)method.invoke(calculadora, firstNumber, secondNumber);
			
			method = calculadoraClass.getMethod("division", int.class, int.class);
			resultDivision = (int)method.invoke(calculadora, firstNumber, secondNumber);
			
			resultadosString = new StringBuilder();
			resultadosString.append("\nSuma:" ).append(resultSuma);
			resultadosString.append("\nResta: " ).append(resultResta);
			resultadosString.append("\nMulti: " ).append(resultMultiplicacion);
			resultadosString.append("\nDivision: " ).append(resultDivision);
			System.out.println("REFLEXION CONSTRUCT!");
			System.out.println(resultadosString.toString());
		
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
		
	
}
